"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = _interopRequireDefault(require("express"));

var _signupConfiramtionRoute = _interopRequireDefault(require("./routers/signupConfiramtionRoute"));

var _superviseCluster = _interopRequireDefault(require("./helpers/superviseCluster"));

var _basicLoginRoute = _interopRequireDefault(require("./routers/basicLoginRoute"));

var _signupRoute = _interopRequireDefault(require("./routers/signupRoute"));

var _jwtLoginRoute = _interopRequireDefault(require("./routers/jwtLoginRoute"));

var _connectDB = _interopRequireDefault(require("./helpers/connectDB"));

var _logoutRoute = _interopRequireDefault(require("./routers/logoutRoute"));

var _appInit = _interopRequireDefault(require("./helpers/appInit"));

var TARGET = process.env.npm_lifecycle_event;
var modeDev = TARGET === 'dev';
(0, _connectDB["default"])(modeDev);
var app = (0, _express["default"])();
(0, _appInit["default"])(app, modeDev);
(0, _signupRoute["default"])(app);
(0, _basicLoginRoute["default"])(app);
(0, _jwtLoginRoute["default"])(app);
(0, _logoutRoute["default"])(app);
(0, _signupConfiramtionRoute["default"])(app);
(0, _superviseCluster["default"])(app, modeDev);
var _default = app;
exports["default"] = _default;