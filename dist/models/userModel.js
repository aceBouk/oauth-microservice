"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

var _bcrypt = _interopRequireDefault(require("bcrypt"));

var _v = _interopRequireDefault(require("uuid/v4"));

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _dotenv = _interopRequireDefault(require("dotenv"));

_dotenv["default"].load();

var SALT_WORK_FACTOR = 10;
var userSchema = new _mongoose["default"].Schema({
  _id_: {
    type: String,
    index: {
      unique: true
    }
  },
  name: [String, String],
  email: {
    type: String,
    lowercase: true,
    validate: {
      validator: function validator(v) {
        return /\S+@\S+\.\S+/.test(v);
      },
      message: '{VALUE} is not a valid email!'
    },
    required: [true, 'User email required'],
    index: {
      unique: true
    }
  },
  password: {
    type: String,
    required: [true]
  },
  admin: Boolean,
  confirmed: {
    type: Boolean,
    "default": false
  },
  creationDate: {
    type: Date,
    required: true,
    "default": Date.now
  },
  lastLogin: {
    type: Date,
    required: true,
    "default": Date.now
  },
  contact: {},
  age: {
    type: Number,
    min: 13,
    max: 120
  },
  phone: {
    type: String,
    validate: {
      validator: function validator(v) {
        return /\d{3}-\d{3}-\d{4}/.test(v);
      },
      message: '{VALUE} is not a valid phone number!'
    },
    required: [false, 'User phone number required']
  },
  token: {
    type: String,
    index: {
      unique: true
    }
  },
  google: {
    type: Object
  }
});
userSchema.pre('save', function (next) {
  //eslint-disable-line
  var user = this;
  var secretKey = (0, _v["default"])();
  user._id_ = secretKey;
  var startDate = '28 Sep 2018';
  var numOfYears = 1;
  var expireDate = new Date(startDate);
  expireDate.setFullYear(expireDate.getFullYear() + numOfYears);
  expireDate.setDate(expireDate.getDate() - 1);
  var expirationDate = Date.parse(expireDate);
  var jwtOpts = {
    expiresIn: expirationDate
  };

  var jwToken = _jsonwebtoken["default"].sign({
    data: user._id_
  }, "".concat(process.env.SECRET_KEY), jwtOpts);

  user.token = jwToken;

  _bcrypt["default"].genSalt(SALT_WORK_FACTOR, function (err, salt) {
    //eslint-disable-line
    if (err) {
      return next(err);
    }

    _bcrypt["default"].hash(user.password, salt, function (error, hash) {
      if (error) {
        return next(err);
      }

      user.password = hash;
      return next();
    });
  });
});

userSchema.methods.verifyPassword = function (password, cb) {
  _bcrypt["default"].compare(password, this.password, function (err, isMatch) {
    if (err) return cb(err);
    return cb(null, isMatch);
  });
};

var _default = _mongoose["default"].model('User', userSchema);

exports["default"] = _default;