"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _basicAuth = _interopRequireDefault(require("../controllers/basicAuth"));

var basicLogin = function basicLogin(app) {
  app.post('/login', _basicAuth["default"], function (req, res, next) {
    req.session.save(function (err) {
      if (err) {
        return next(err);
      }

      res.statusCode = 200;
      return res.send(req.user);
    });
  });
};

var _default = basicLogin;
exports["default"] = _default;