"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _passport = _interopRequireDefault(require("passport"));

var _dotenv = _interopRequireDefault(require("dotenv"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// import User from '../models/userModel';
var GoogleStrategy = require('passport-google-oauth2').Strategy;

_dotenv["default"].load();

var googleOauth = function googleOauth(app) {
  var oauth2 = {
    google: {
      clientID: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      callbackURL: process.env.GOOGLE_AUTH_CALLBACK_URL
    }
  };

  _passport["default"].serializeUser(function (user, done) {
    var sessionUser = {
      id: user.id,
      name: user.name,
      email: user.email,
      photoUrl: user.photos[0].value
    };
    done(null, sessionUser);
  });

  _passport["default"].deserializeUser(function (sessionUser, done) {
    done(null, sessionUser);
  });

  _passport["default"].use(new GoogleStrategy({
    clientID: oauth2.google.clientID,
    clientSecret: oauth2.google.clientSecret,
    callbackURL: oauth2.google.callbackURL
  }, function (accessToken, refreshToken, profile, done) {
    process.nextTick(function () {
      if (profile) {
        done(null, profile); // User.findOne({ googleId: profile.id }, (err, user) => {
        //   if (err) {
        //     return done(err);
        //   }
        //   if (user) {
        //     return done(null, user);
        //   } else {
        //     const googleUser = {
        //       google: {
        //         id: profile.id,
        //         token: accessToken,
        //         name: profile.displayName,
        //         email: profile.emails[0].value
        //       }
        //     };
        //     const newUser = new User(googleUser);
        //     // save the user
        //     newUser.save()
        //       .then((res) => {
        //         return done(null, profile);
        //       })
        //       .catch(error => {
        //         throw error;
        //       });
        //   }
        //   return done(null, null);
        // });
      }
    });
  }));

  app.get('/auth/google', _passport["default"].authenticate('google', {
    scope: ['https://www.googleapis.com/auth/userinfo.email', 'https://www.googleapis.com/auth/userinfo.profile', 'https://www.googleapis.com/auth/plus.me']
  })); // app.get('/auth/google/callback',
  //   passport.authenticate('google', { successRedirect: '/account',
  //     failureRedirect: '/error' }));
  // app.get('/auth/google/callback', (req, res) => {
  //   passport.authenticate('google', (user, info) => {
  //     res.json(req.session);
  //   });
  // });

  app.get('/auth/google/callback', _passport["default"].authenticate('google'), function (req, res) {
    res.json(req.session);
  });
};

var _default = googleOauth;
exports["default"] = _default;