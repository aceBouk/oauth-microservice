"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _logout = _interopRequireDefault(require("../controllers/logout"));

var logoutRoute = function logoutRoute(app) {
  app.get('/logout', function (req, res) {
    (0, _logout["default"])(req.user);
    req.session.destroy(function (err) {
      if (err) {
        throw err;
      } else {
        res.redirect('/');
      }
    });
  });
};

var _default = logoutRoute;
exports["default"] = _default;