"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _signUp = _interopRequireDefault(require("../controllers/signUp"));

var _sendEmail = _interopRequireDefault(require("../helpers/sendEmail"));

var signupRoute = function signupRoute(app) {
  app.post('/signup', function (req, res) {
    var user = {
      email: req.body.email,
      name: req.body.name || [],
      password: req.body.password,
      age: req.body.age,
      phone: req.body.phone
    };
    (0, _signUp["default"])(user).then(function (result) {
      var url = "http://".concat(req.hostname, ":").concat(process.env.PORT, "/confirmation/").concat(result.token);
      (0, _sendEmail["default"])(result.email, url);
      res.status = 200;
      res.send("CONFIRMATION URL : http://localhost:2000/confirmation/".concat(result.token));
      return result;
    })["catch"](function (err) {
      throw err;
    });
  });
};

var _default = signupRoute;
exports["default"] = _default;