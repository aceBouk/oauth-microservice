"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _jwtAuth = _interopRequireDefault(require("../controllers/jwtAuth"));

var JWTLogin = function JWTLogin(app) {
  app.post('/login/jwt/', _jwtAuth["default"], function (req, res, next) {
    req.session.save(function (err) {
      if (err) {
        return next(err);
      }

      return res.send(req.user);
    });
  });
};

var _default = JWTLogin;
exports["default"] = _default;