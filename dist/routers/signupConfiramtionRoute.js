"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _signUpConfirmation = _interopRequireDefault(require("../controllers/signUpConfirmation"));

var signupConfirmationRoute = function signupConfirmationRoute(app) {
  app.get('/confirmation/:token', function (req, res) {
    (0, _signUpConfirmation["default"])(req.params.token);
  });
};

var _default = signupConfirmationRoute;
exports["default"] = _default;