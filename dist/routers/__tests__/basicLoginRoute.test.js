"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _express = _interopRequireDefault(require("express"));

var _supertest = _interopRequireDefault(require("supertest"));

var _basicLoginRoute = _interopRequireDefault(require("../basicLoginRoute"));

var app = (0, _express["default"])();
(0, _basicLoginRoute["default"])(app);
var token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjoiZDMyZTBkZDgtYjcyOC00ZTYwLWEzNjEtNzk4YTMzMGFlMzhhIiwiaWF0IjoxNTMzNzUwNDQ3LCJleHAiOjMwNjc1MDQ0OTR9.Jm5odfasaTjSfUbS-EtEn-OSXEuUTAbJlZAIFWrC9cM';
beforeAll(function () {
  var httpServer = app.listen();
});
describe('test basic login route', function () {
  it('should response post methode',
  /*#__PURE__*/
  function () {
    var _ref = (0, _asyncToGenerator2["default"])(
    /*#__PURE__*/
    _regenerator["default"].mark(function _callee(done) {
      var response;
      return _regenerator["default"].wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return (0, _supertest["default"])(app).post('/login').set('Authorization, token');

            case 2:
              response = _context.sent;
              expect(response.statusCode).toBe(200);
              done(); // request.post('/login')
              //   .set('Authorization', token)
              //   .then((response) => {
              //     expect(response.statusCode).toBe(501);
              //     done();
              //   });
              // httpServer.close();

            case 5:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function (_x) {
      return _ref.apply(this, arguments);
    };
  }());
});