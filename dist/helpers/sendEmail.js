"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mail = _interopRequireDefault(require("@sendgrid/mail"));

var sendEmail = function sendEmail(email, url) {
  _mail["default"].setApiKey(process.env.SENDGRID_API_KEY);

  var msg = {
    to: email,
    from: 'noreply@snutsh.com',
    subject: 'Signup confirmation email',
    text: 'Your confirmation url',
    html: "<strong>Your confirmation url is : ".concat(url, "</strong>")
  };

  _mail["default"].send(msg);
};

var _default = sendEmail;
exports["default"] = _default;