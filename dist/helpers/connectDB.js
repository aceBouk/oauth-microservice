"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

var _safe = _interopRequireDefault(require("colors/safe"));

var _dotenv = _interopRequireDefault(require("dotenv"));

_dotenv["default"].load();

var connectDB = function connectDB() {
  _mongoose["default"].connect(process.env.DB, {
    useNewUrlParser: true
  });

  _mongoose["default"].connection.on('connected', function () {
    console.log(_safe["default"].magenta("Mongoose default connection is open to ".concat(process.env.DB))); //eslint-disable-line
  });

  _mongoose["default"].connection.on('error', function (err) {
    console.log(_safe["default"].red("Mongoose default connection has occured ".concat(err, " error"))); //eslint-disable-line
  });

  _mongoose["default"].connection.on('disconnected', function () {
    console.log(_safe["default"].blue('Mongoose default connection is disconnected')); //eslint-disable-line
  });

  process.on('SIGINT', function () {
    _mongoose["default"].connection.close(function () {
      console.log(_safe["default"].blue('Mongoose default connection is disconnected due to application termination')); //eslint-disable-line

      process.exit(0);
    });
  });
};

var _default = connectDB;
exports["default"] = _default;