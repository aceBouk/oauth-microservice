"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _cluster = _interopRequireDefault(require("cluster"));

var _os = _interopRequireDefault(require("os"));

var _safe = _interopRequireDefault(require("colors/safe"));

var numCPUs = _os["default"].cpus().length;

var superviseCluster = function superviseCluster(app, modeDev) {
  if (_cluster["default"].isMaster) {
    console.log(_safe["default"].yellow("Master ".concat(process.pid, " is running"))); //eslint-disable-line

    for (var i = 0; i < numCPUs; i++) {
      _cluster["default"].fork();
    }

    _cluster["default"].on('exit', function (worker, code, signal) {
      console.log(_safe["default"].yellow("worker ".concat(worker.process.pid, " died"))); //eslint-disable-line
    });
  } else {
    app.listen(app.get('port'), function (err) {
      if (err) {
        throw err;
      }

      if (modeDev) {
        console.log(_safe["default"].green("<<<   server is linstening on port ".concat(app.get('port'), " enjoy with \u2665   >>>"))); //eslint-disable-line
      }
    });
  }

  process.on('uncaughtException', function (err) {
    console.error(_safe["default"].red("".concat(new Date().toUTCString(), " uncaughtException: ").concat(err.message))); //eslint-disable-line

    console.error(_safe["default"].gray(err.stack)); //eslint-disable-line

    process.exit(1);
  });
};

var _default = superviseCluster;
exports["default"] = _default;