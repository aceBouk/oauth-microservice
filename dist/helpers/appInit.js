"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _expressSession = _interopRequireDefault(require("express-session"));

var _cookieParser = _interopRequireDefault(require("cookie-parser"));

var _bodyParser = _interopRequireDefault(require("body-parser"));

var _passport = _interopRequireDefault(require("passport"));

var _morgan = _interopRequireDefault(require("morgan"));

var _dotenv = _interopRequireDefault(require("dotenv"));

var _connectFlash = _interopRequireDefault(require("connect-flash"));

var _redis = _interopRequireDefault(require("redis"));

var _connectRedis = _interopRequireDefault(require("connect-redis"));

var RedisStore = (0, _connectRedis["default"])(_expressSession["default"]);

var ExpressInit = function ExpressInit(app, modeDev) {
  _dotenv["default"].load();

  var client = _redis["default"].createClient();

  var redisOpts = {
    client: client,
    host: process.env.HOST,
    port: process.env.REDIS_PORT,
    ttl: process.env.REDIS_TTL
  };
  var sessionOpts = {
    store: new RedisStore(redisOpts),
    saveUninitialized: true,
    resave: true,
    secret: 'BestProduct',
    cookie: {
      httpOnly: true,
      maxAge: 2419200000
    }
  };
  app.set('port', process.env.PORT || 3001);

  if (modeDev) {
    app.use((0, _morgan["default"])('dev'));
  }

  app.use((0, _connectFlash["default"])());
  app.use((0, _cookieParser["default"])());
  app.use((0, _expressSession["default"])(sessionOpts));
  app.use(_passport["default"].initialize());
  app.use(_passport["default"].session());
  app.use(_bodyParser["default"].json());
  app.use(_bodyParser["default"].urlencoded({
    extended: false
  }));
};

var _default = ExpressInit;
exports["default"] = _default;