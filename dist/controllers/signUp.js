"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _userModel = _interopRequireDefault(require("../models/userModel"));

var signUp = function signUp(user) {
  var newUser = new _userModel["default"](user);
  return new Promise(function (resolve, reject) {
    _userModel["default"].findOne({
      email: newUser.email
    }, function (err, docs) {
      if (err) {
        throw new Error(err.message);
      }

      if (docs) {
        throw new Error("".concat(docs.email, " exists already"));
      } else {
        newUser.save().then(function (result) {
          return resolve(result);
        })["catch"](function (error) {
          reject(error);
        });
      }
    });
  });
};

var _default = signUp;
exports["default"] = _default;