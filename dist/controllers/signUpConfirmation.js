"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _userModel = _interopRequireDefault(require("../models/userModel"));

var _dotenv = _interopRequireDefault(require("dotenv"));

_dotenv["default"].load();

var signUpConfirmation = function signUpConfirmation(token) {
  _jsonwebtoken["default"].verify(token, "".concat(process.env.SECRET_KEY), function (err, decoded) {
    if (err) {
      throw err;
    }

    _userModel["default"].findOneAndUpdate({
      _id_: decoded.data
    }, {
      $set: {
        confirmed: true
      }
    }).then(function (result) {
      return result;
    })["catch"](function (error) {
      throw error;
    });
  });
};

var _default = signUpConfirmation;
exports["default"] = _default;