"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var updateUserLogout = function updateUserLogout(user) {
  UserModel.findOneAndUpdate({
    _id_: user._id_
  }, {
    lastLogin: Date.now()
  }, function (err, doc) {
    if (err) {
      throw err;
    }

    return doc;
  });
};

var _default = updateUserLogout;
exports["default"] = _default;