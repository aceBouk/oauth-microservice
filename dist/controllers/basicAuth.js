"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _passport = _interopRequireDefault(require("passport"));

var _passportHttp = _interopRequireDefault(require("passport-http"));

var _userModel = _interopRequireDefault(require("../models/userModel"));

var BasicStrategy = _passportHttp["default"].BasicStrategy;

var basicValidation = function basicValidation(username, password, callback) {
  _userModel["default"].findOne({
    email: username
  }, function (err, user) {
    //eslint-disable-line
    if (err) {
      throw err;
    }

    if (!user) {
      return callback(null, false);
    }

    user.verifyPassword(password, function (error, isMatch) {
      if (error) {
        return callback(err);
      }

      if (!isMatch) {
        return callback(null, false);
      }

      user.password = undefined;
      return callback(null, user);
    });
  });
};

_passport["default"].serializeUser(function (user, done) {
  done(null, user._id_);
});

_passport["default"].deserializeUser(function (id, done) {
  _userModel["default"].findOne({
    _id_: id
  }).then(function (user) {
    done(null, user);
  })["catch"](function (err) {
    throw err;
  });
});

_passport["default"].use('basic', new BasicStrategy(basicValidation));

var isAuthenticated = _passport["default"].authenticate('basic', {
  session: true
});

var _default = isAuthenticated;
exports["default"] = _default;