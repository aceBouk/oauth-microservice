"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _passport = _interopRequireDefault(require("passport"));

var _passportJwt = _interopRequireDefault(require("passport-jwt"));

var _userModel = _interopRequireDefault(require("../models/userModel"));

var ExtractJWT = _passportJwt["default"].ExtractJwt;
var JWTStrategy = _passportJwt["default"].Strategy;

_passport["default"].use(new JWTStrategy({
  jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
  secretOrKey: "".concat(process.env.SECRET_KEY)
}, function (jwtPayload, cb) {
  return _userModel["default"].findOne({
    _id_: jwtPayload.data
  }).then(function (user) {
    user.password = undefined;
    return cb(null, user);
  })["catch"](function (err) {
    return cb(err);
  });
}));

_passport["default"].serializeUser(function (user, done) {
  done(null, user._id_);
});

_passport["default"].deserializeUser(function (id, done) {
  _userModel["default"].findById(id).then(function (user) {
    done(null, user);
  })["catch"](function (err) {
    throw err;
  });
});

var JWTisAuthenticated = _passport["default"].authenticate('jwt', {
  session: true
});

var _default = JWTisAuthenticated;
exports["default"] = _default;