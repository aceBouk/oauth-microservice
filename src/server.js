import express from 'express';
import signupConfirmationRoute from './routers/signupConfiramtionRoute';
import superviseCluster from './helpers/superviseCluster';
import basicLogin from './routers/basicLoginRoute';
import signupRouter from './routers/signupRoute';
import jwtLogin from './routers/jwtLoginRoute';
import connectDB from './helpers/connectDB';
import logout from './routers/logoutRoute';
import initApp from './helpers/appInit';

const TARGET = process.env.npm_lifecycle_event;
const modeDev = TARGET === 'dev';
connectDB(modeDev);

const app = express();
initApp(app, modeDev);
signupRouter(app);
basicLogin(app);
jwtLogin(app);
logout(app);
signupConfirmationRoute(app);
superviseCluster(app, modeDev);

export default app;
