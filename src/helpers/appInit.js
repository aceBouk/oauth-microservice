// @flow

import session from 'express-session';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import passport from 'passport';
import logger from 'morgan';
import dotenv from 'dotenv';
import flash from 'connect-flash';
import redis from 'redis';
import redisConnect from 'connect-redis';
const RedisStore = redisConnect(session);

const ExpressInit = (app: {use: Function, set: Function}, modeDev: boolean): void => {
  dotenv.load();
  const client = redis.createClient();
  const redisOpts: {
    client: Function,
    host: string,
    port: string,
    ttl: string
  } = {
    client: client,
    host: process.env.HOST,
    port: process.env.REDIS_PORT,
    ttl: process.env.REDIS_TTL
  };
  const sessionOpts: {
    store: Function,
    saveUninitialized: boolean,
    resave: boolean,
    secret: string,
    cookie: {httpOnly: boolean, maxAge: number}
  } = {
    store: new RedisStore(redisOpts),
    saveUninitialized: true,
    resave: true,
    secret: 'BestProduct',
    cookie: { httpOnly: true, maxAge: 2419200000 }
  };
  app.set('port', process.env.PORT || 3001);

  if (modeDev) {
    app.use(logger('dev'));
  }
  app.use(flash());
  app.use(cookieParser());
  app.use(session(sessionOpts));
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
};

export default ExpressInit;
