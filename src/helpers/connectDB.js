import mongoose from 'mongoose';
import colors from 'colors/safe';
import dotenv from 'dotenv';

dotenv.load();

const connectDB = () => {
  mongoose.connect(process.env.DB, { useNewUrlParser: true });
  mongoose.connection.on('connected', () => {
    console.log(colors.magenta(`Mongoose default connection is open to ${process.env.DB}`)); //eslint-disable-line
  });

  mongoose.connection.on('error', (err) => {
    console.log(colors.red(`Mongoose default connection has occured ${err} error`)); //eslint-disable-line
  });

  mongoose.connection.on('disconnected', () => {
    console.log(colors.blue('Mongoose default connection is disconnected')); //eslint-disable-line
  });

  process.on('SIGINT', () => {
    mongoose.connection.close(() => {
      console.log(colors.blue('Mongoose default connection is disconnected due to application termination')); //eslint-disable-line
      process.exit(0);
    });
  });
};

export default connectDB;
