// @flow

const isLoggedIn = (req: Object, res: Object, next: Function): Function => {
  if (req.isAuthenticated()) {
    return next();
  }
  req.session.returnTo = req.originalUrl;
  return res.redirect('/login');
};

export default isLoggedIn;
