// @flow

import sgMail from '@sendgrid/mail';

const sendEmail = (email: string, url: string): void => {
  sgMail.setApiKey(process.env.SENDGRID_API_KEY);
  const msg = {
    to: email,
    from: 'noreply@snutsh.com',
    subject: 'Signup confirmation email',
    text: 'Your confirmation url',
    html: `<strong>Your confirmation url is : ${url}</strong>`
  };
  sgMail.send(msg);
};

export default sendEmail;
