// @flow

import cluster from 'cluster';
import os from 'os';
import colors from 'colors/safe';
const numCPUs = os.cpus().length;

const superviseCluster = (app: {listen: Function}, modeDev: boolean): void => {
  if (cluster.isMaster) {
    console.log(colors.yellow(`Master ${process.pid} is running`)); //eslint-disable-line

    for (let i = 0; i < numCPUs; i++) {
      cluster.fork();
    }

    cluster.on('exit', (worker, code, signal) => {
      console.log(colors.yellow(`worker ${worker.process.pid} died`)); //eslint-disable-line
    });
  } else {
    app.listen(app.get('port'), (err) => {
      if (err) {
        throw err;
      }
      if (modeDev) {
        console.log(colors.green(`<<<   server is linstening on port ${app.get('port')} enjoy with ♥   >>>`)); //eslint-disable-line
      }
    });
  }

  process.on('uncaughtException', function (err) {
    console.error(colors.red(`${(new Date()).toUTCString()} uncaughtException: ${err.message}`)); //eslint-disable-line
    console.error(colors.gray(err.stack)); //eslint-disable-line
    process.exit(1);
  });
};

export default superviseCluster;
