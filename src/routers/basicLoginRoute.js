// @flow

import isAuthenticated from '../controllers/basicAuth';

const basicLogin = (app: {post: Function}): Function => {
  app.post('/login', isAuthenticated, (req: Object, res: Object, next: Function) => {
    req.session.save((err) => {
      if (err) {
        return next(err);
      }
      res.statusCode = 200;
      return res.send(req.user);
    });
  });
};

export default basicLogin;
