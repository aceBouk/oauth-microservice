import express from 'express';
import request from 'supertest';
import basicLogin from '../basicLoginRoute';

const app = express();
basicLogin(app);
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjoiZDMyZTBkZDgtYjcyOC00ZTYwLWEzNjEtNzk4YTMzMGFlMzhhIiwiaWF0IjoxNTMzNzUwNDQ3LCJleHAiOjMwNjc1MDQ0OTR9.Jm5odfasaTjSfUbS-EtEn-OSXEuUTAbJlZAIFWrC9cM';

beforeAll(() => {
  const httpServer = app.listen();
});

describe('test basic login route', () => {
  it('should response post methode', async (done) => {
    const response = await request(app).post('/login').set('Authorization, token');
    expect(response.statusCode).toBe(200);
    done();
    // request.post('/login')
    //   .set('Authorization', token)
    //   .then((response) => {
    //     expect(response.statusCode).toBe(501);
    //     done();
    //   });
    // httpServer.close();
  });
});
