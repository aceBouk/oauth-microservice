// @flow

import signUp from '../controllers/signUp';
import sendEmail from '../helpers/sendEmail';

const signupRoute = (app: {post: Function}) => {
  app.post('/signup', (req: Object, res: Object) => {
    const user: {
      email: string,
      name: string | string[],
      password: string,
      age: number,
      phone: string
    } = {
      email: req.body.email,
      name: req.body.name || [],
      password: req.body.password,
      age: req.body.age,
      phone: req.body.phone
    };
    signUp(user)
      .then((result: Object) => {
        const url = `http://${req.hostname}:${process.env.PORT}/confirmation/${result.token}`;
        sendEmail(result.email, url);
        res.status = 200;
        res.send(`CONFIRMATION URL : http://localhost:2000/confirmation/${result.token}`);
        return result;
      })
      .catch((err: Object) => {
        throw err;
      });
  });
};

export default signupRoute;
