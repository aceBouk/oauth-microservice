// @flow

import signUpConfiramtion from '../controllers/signUpConfirmation';

const signupConfirmationRoute = (app: {get: Function}): void => {
  app.get('/confirmation/:token', (req: Object, res: Object): void => {
    signUpConfiramtion(req.params.token);
  });
};

export default signupConfirmationRoute;
