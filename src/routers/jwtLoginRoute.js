// @flow

import JWTisAuthenticated from '../controllers/jwtAuth';

const JWTLogin = (app: {post: Function}): Function => {
  app.post('/login/jwt/', JWTisAuthenticated, (req: Object, res: Object, next: Function): Function => {
    req.session.save((err: Object): Object => {
      if (err) {
        return next(err);
      }
      return res.send(req.user);
    });
  });
};

export default JWTLogin;
