// @flow

import updateUserLogout from '../controllers/logout';

const logoutRoute = (app: {get: Function}): void => {
  app.get('/logout', (req: Object, res: Object): void => {
    updateUserLogout(req.user);
    req.session.destroy((err: Object): void => {
      if (err) {
        throw (err);
      } else {
        res.redirect('/');
      }
    });
  });
};

export default logoutRoute;
