// @flow

import typeof UserModel from '../models/userModel';
const updateUserLogout = (user: Object): Object => {
  UserModel.findOneAndUpdate({_id_: user._id_}, {lastLogin: Date.now()}, (err, doc) => {
    if (err) {
      throw err;
    }
    return doc;
  });
};

export default updateUserLogout;
