import passport from 'passport';
import passportJWT from 'passport-jwt';
import User from '../models/userModel';

const ExtractJWT = passportJWT.ExtractJwt;
const JWTStrategy = passportJWT.Strategy;

passport.use(new JWTStrategy({
  jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
  secretOrKey: `${process.env.SECRET_KEY}`
}, (jwtPayload, cb) => {
  return User.findOne({ _id_: jwtPayload.data })
    .then(user => {
      user.password = undefined;
      return cb(null, user);
    })
    .catch(err => {
      return cb(err);
    });
}
));

passport.serializeUser((user, done) => {
  done(null, user._id_);
});

passport.deserializeUser((id, done) => {
  User.findById(id)
    .then((user) => {
      done(null, user);
    })
    .catch((err) => {
      throw err;
    });
});

const JWTisAuthenticated = passport.authenticate('jwt', {session: true});
export default JWTisAuthenticated;
