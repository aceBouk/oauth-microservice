// @flow

import UserModel from '../models/userModel';
const signUp = (user: Object): Promise<any> => {
  const newUser = new UserModel(user);
  return new Promise((resolve: Function, reject: Function): Function => {
    UserModel.findOne({email: newUser.email}, (err, docs) => {
      if (err) {
        throw new Error(err.message);
      }
      if (docs) {
        throw new Error(`${docs.email} exists already`);
      } else {
        newUser.save()
          .then((result) => {
            return resolve(result);
          })
          .catch((error) => {
            reject(error);
          });
      }
    });
  });
};

export default signUp;
