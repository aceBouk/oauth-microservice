// @flow

import jwt from 'jsonwebtoken';
import UserModel from '../models/userModel';
import dotenv from 'dotenv';
dotenv.load();

const signUpConfirmation = (token: string): void => {
  jwt.verify(token, `${process.env.SECRET_KEY}`, (err, decoded) => {
    if (err) {
      throw err;
    }
    UserModel.findOneAndUpdate({_id_: decoded.data}, {$set: { confirmed: true }})
      .then((result) => {
        return result;
      })
      .catch((error) => {
        throw error;
      });
  });
};

export default signUpConfirmation;
