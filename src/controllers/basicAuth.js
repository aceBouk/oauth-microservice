// @flow

import passport from 'passport';
import passportLocal from 'passport-http';
import User from '../models/userModel';

const BasicStrategy = passportLocal.BasicStrategy;

const basicValidation = (username: string, password: string, callback: Function): void => {
  User.findOne({ email: username }, (err, user) => { //eslint-disable-line
    if (err) {
      throw err;
    }
    if (!user) {
      return callback(null, false);
    }
    user.verifyPassword(password, (error: Object, isMatch: boolean) => {
      if (error) { return callback(err); }
      if (!isMatch) { return callback(null, false); }
      user.password = undefined;
      return callback(null, user);
    });
  });
};

passport.serializeUser((user, done) => {
  done(null, user._id_);
});

passport.deserializeUser((id, done) => {
  User.findOne({_id_: id})
    .then((user) => {
      done(null, user);
    })
    .catch((err) => {
      throw err;
    });
});

passport.use('basic', new BasicStrategy(basicValidation));
const isAuthenticated = passport.authenticate('basic', {session: true});

export default isAuthenticated;
