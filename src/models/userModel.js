import Mongoose from 'mongoose';
import bcrypt from 'bcrypt';
import uuidV4 from 'uuid/v4';
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';

dotenv.load();
const SALT_WORK_FACTOR = 10;

const userSchema = new Mongoose.Schema({
  _id_: {
    type: String,
    index: { unique: true }
  },
  name: [String, String],
  email: {
    type: String,
    lowercase: true,
    validate: {
      validator:
        (v) => {
          return /\S+@\S+\.\S+/.test(v);
        },
      message: '{VALUE} is not a valid email!'
    },
    required: [true, 'User email required'],
    index: { unique: true }
  },
  password: {
    type: String,
    required: [true]
  },
  admin: Boolean,
  confirmed: {
    type: Boolean,
    default: false
  },
  creationDate: { type: Date, required: true, default: Date.now },
  lastLogin: { type: Date, required: true, default: Date.now },
  contact: {},
  age: { type: Number, min: 13, max: 120 },
  phone: {
    type: String,
    validate: {
      validator:
        (v) => {
          return /\d{3}-\d{3}-\d{4}/.test(v);
        },
      message: '{VALUE} is not a valid phone number!'
    },
    required: [false, 'User phone number required']
  },
  token: {
    type: String,
    index: { unique: true }
  },
  google: {
    type: Object
  }
});

userSchema.pre('save', function (next) { //eslint-disable-line
  const user = this;
  const secretKey = uuidV4();
  user._id_ = secretKey;
  const startDate = '28 Sep 2018';
  const numOfYears = 1;
  let expireDate = new Date(startDate);
  expireDate.setFullYear(expireDate.getFullYear() + numOfYears);
  expireDate.setDate(expireDate.getDate() - 1);
  const expirationDate = Date.parse(expireDate);
  const jwtOpts = {
    expiresIn: expirationDate
  };
  const jwToken = jwt.sign({data: user._id_}, process.env.SECRET_KEY, jwtOpts);
  user.token = jwToken;
  bcrypt.genSalt(SALT_WORK_FACTOR, (err, salt) => { //eslint-disable-line
    if (err) {
      return next(err);
    }

    bcrypt.hash(user.password, salt, (error, hash) => {
      if (error) {
        return next(err);
      }

      user.password = hash;
      return next();
    });
  });
});

userSchema.methods.verifyPassword = function (password, cb) {
  bcrypt.compare(password, this.password, (err, isMatch) => {
    if (err) return cb(err);
    return cb(null, isMatch);
  });
};

export default Mongoose.model('User', userSchema);
